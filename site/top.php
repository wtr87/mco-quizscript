<html>
<head>
   <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
   <link href="css.css" rel="stylesheet" type="text/css">
   <link href="questionStyle.css" rel="stylesheet" type="text/css">
   <script type="text/javascript" src="js.js"></script>
   <script src="http://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
   <script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
   ga('create', 'UA-46145849-1', 'uva.nl');
   ga('send', 'pageview');
   </script>
   <link rel="shortcut icon" href="icon.ico">
   <title>Interactief Werkboek MCO/BS</title>
</head>
<body>

<!-- Main Table Opening -->

<table border="0">
  <tr><td valign="top" class="nav">

<!-- Nav Opening -->

<!-- Nav Header -->

<A HREF="index.php"><IMG SRC="./images/bs.png" border="0"></a>
<p class="small">&nbsp;Universiteit van Amsterdam<br/>&nbsp;MCO/BS Interactief Werkboek</p>

<!-- 0 Home -->

  <div class="button"><A HREF="index.php"><B>Home</B></a></div>
  <div style="clear:both;" />

  <div class="button"><A HREF="spss.php"><B>Over SPSS</B></a></div>
  <div style="clear:both;" />

  <div class="button"><A HREF="keuze.php"><B>Keuzeschema's</B></a></div>
  <div style="clear:both;" />