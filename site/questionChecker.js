/*
QUESTION CHECKER SCRIPT
Door: Wouter Stuifmeel
UvA - ICTO Communicatiewetenschappen
*/

function checkAnswer(that, answerID, answerKey) {
  var answerInput = $(that).closest(".question").find(".answer-textarea").val();
  var answerRegExp = new RegExp(answerKey);
  var answerRes = answerRegExp.test(answerInput);

  if (answerRes) {
    $(that).closest(".question").find(answerID).filter(".right").fadeToggle();
  } else {
    $(that).closest(".question").find(answerID).filter(".wrong").fadeToggle();
  }
};

$( document ).ready(function() {
  $( ".questionButton" ).click(function() {
    var that = this;

    $(that).closest(".question").find(".right").css("display", "none");
    $(that).closest(".question").find(".wrong").css("display", "none");

    var answerIDs = $(this).closest(".question").find(".answerID").map(function() {return this.value;}).get();

    answerIDs.map( function(item) {
      var itemClass = "." + item;
      var answerKey = $(that).closest(".question").find(".answerKey").filter(itemClass).val();
      checkAnswer(that, itemClass, answerKey);
    });

  });
});