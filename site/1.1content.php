<h1>SPSS opgave 1.1</h1>

<p>Open het databestand <a href="./data/televisie.sav" class="inlink">Televisie.sav</a> en klik op het tabblad Data View. Als het goed is krijg je het volgende te zien.</p>

<img src="./images/1.1.png">

<div class="question">
  <p><b>a.</b> Wat zijn de onderzoekseenheden in dit onderzoek?</p>
  <textarea name="answer" rows="4" class="answer-textarea"></textarea>
  <div class="questionButton"><a href="#">Verstuur</a></div>

  <div class="answer right 1-1">
  <p>Inderdaad, de onderzoekseenheden zijn de mensen/jongeren die als respondenten aan het onderzoek hebben meegewerkt.</p>
  </div>
  <div class="answer wrong 1-1">
  <p>Het lijkt erop dat je niet het juiste antwoord hebt gegeven. De onderzoekseenheden zijn hier mensen. Het zijn immers mensen die televisie kijken, een leeftijd hebben en man of vrouw zijn. Verder weten we niets over deze mensen.</p>
  </div>

  <input type="hidden" class="answerID" value="1-1">
  <input type="hidden" class="answerKey 1-1" value="mensen|respondenten|jongeren|personen|persoon|studenten">
</div>

<p>Wanneer we werken in SPSS gebruiken we standaardbegrippen (terminologie van SPSS). Kies bij je antwoord op de volgende vragen steeds uit een van de volgende begrippen: valuelabel, waarde of value, variabele(n), variabelenaam, label, onderzoekseenheid/onderzoekseenheden.</p>

<p><b>b.</b> Hoe noemen we de 0 in de cel linksboven?
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=2>
<PARAM NAME="testen" VALUE=1>
<PARAM NAME="l1" VALUE="s1^waarde^value^">
<PARAM NAME="l2" VALUE="s2^valuel^">
<PARAM NAME="l3" VALUE="t1^j^n^Inderdaad, elke code in een cel is een waarde of value.^Je hebt niet het juiste begrip gekozen. Probeer het nog eens of bekijk de hints nog eens.">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<p><b>c.</b> In de kolommen staat onder andere 'leeftijd' en 'sekse'. Hoe noemen we deze begrippen?
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=2>
<PARAM NAME="testen" VALUE=1>
<PARAM NAME="l1" VALUE="s1^variabele^kenmerk^">
<PARAM NAME="l2" VALUE="s2^variabelena^">
<PARAM NAME="l3" VALUE="t1^j^n^Inderdaad, leeftijd en sekse zijn variabelen want mensen kunnen hierin verschillen.^Je hebt niet het juiste begrip gekozen. Leeftijd en sekse zijn kenmerken waarop mensen kunnen verschillen, hoe noemen we dit in de statistiek?">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<p><b>d.</b> In de rijen van 1 tot en met 40 staat de informatie over de...
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=1>
<PARAM NAME="testen" VALUE=1>
<PARAM NAME="l1" VALUE="s1^onderzoekseenh^">
<PARAM NAME="l2" VALUE="t1^j^Dat klopt, de informatie gaat over de onderzoekseenheden.^Je hebt niet het juiste begrip gekozen. Bekijk de hints nogmaals.">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<p><b>e.</b> De 0 in rij vier geeft aan dat de respondent een man is. Is 'man' een value, een valuelabel, een variabelenaam of een label?
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=1>
<PARAM NAME="testen" VALUE=1>
<PARAM NAME="l1" VALUE="s1^valuelabel^value label^">
<PARAM NAME="l2" VALUE="t1^j^'Man' is inderdaad een valuelabel. Het is een beschrijving van de waarde (value) 1.^Je hebt niet het juiste begrip gekozen. Denk eraan dat 'man' een beschrijving is van de waarde (value) 1. Kijk in de Variable View.">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<p><b>f.</b> Hoe noemen we de 23 in de tweede kolom van rij 9?
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=2>
<PARAM NAME="testen" VALUE=2>
<PARAM NAME="l1" VALUE="s1^waarde^value^">
<PARAM NAME="l2" VALUE="s2^valuelabel^value label^">
<PARAM NAME="l3" VALUE="t1^j^n^Inderdaad, 23 is de waarde (value) van persoon 9 op de variabele leeftijd. Kortom, persoon 9 is 23 jaar oud.^Je hebt niet het juiste begrip gekozen. Denk terug aan opgave 1.b.">
<PARAM NAME="l4" VALUE="t2^i^n^^Je hebt het over valuelabels, daar gaat het hier niet om.">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<p><b>g.</b> Wat is de sekse, leeftijd en het favoriete televisieprogramma van respondent 4?
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=3>
<PARAM NAME="testen" VALUE=3>
<PARAM NAME="l1" VALUE="s1^man^">
<PARAM NAME="l2" VALUE="s2^19^negentien^negen tien^">
<PARAM NAME="l3" VALUE="s3^grey^">
<PARAM NAME="l4" VALUE="t1^j^i^i^Sekse = 0 staat hier inderdaad voor 'man' .^Heb je wel aangegeven wat de sekse van deze persoon is?">
<PARAM NAME="l5" VALUE="t2^i^j^i^De persoon in kwestie is inderdaad 19 jaar.^De goede leeftijd komt niet voor in het antwoord.">
<PARAM NAME="l6" VALUE="t3^i^i^j^Het televisieprogramma waar deze persoon het liefst naar kijkt is Grey's Anatomy. Dit kun je onder andere zien bij de Variable View.^Zoek via Variable View welk televisieprogramma bij waarde 1 hoort.">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<p><b>h.</b> Hoeveel waarden kan de variabele 'televisie' aannemen?
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=1>
<PARAM NAME="testen" VALUE=1>
<PARAM NAME="l1" VALUE="s1^4^ vier^">
<PARAM NAME="l2" VALUE="t1^j^Dat klopt, de variabele televisie heeft 4 waarden.^Je hebt niet het goede aantal waarden gegeven. Heb je wel in de Variable View gekeken?">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<p><b>i.</b> Wat is het label van de variabele 'televisie'?
<br>
<APPLET CODE="answerchecker.class" codebase = "./" archive = "." alt = "Your browser understands the &lt;APPLET&gt; tag but isn't running the applet, for some reason." WIDTH=600 HEIGHT=300>
<PARAM NAME="vraag" VALUE="Dit bestaat niet meer">
<PARAM NAME="sleutels" VALUE=1>
<PARAM NAME="testen" VALUE=1>
<PARAM NAME="l1" VALUE="s1^favoriete televisie programma^favoriet televisie programma^favoriete televisieprogramma^favoriet televisieprogramma">
<PARAM NAME="l2" VALUE="t1^j^Inderdaad, het label van de variabele 'televisie' is 'favoriete televisieprogramma'.^Je hebt niet het juiste label gegeven. Om het juiste label te vinden moet je in de Variable View kijken.">
Your browser is ignoring the &lt;APPLET&gt; tag!</APPLET>

<h1>Hints</h1>

  <!-- Tip 1 -->
  1 <b class="hint"><a href="#" onclick="toggleMe('tip1'); return false">Wat zijn variabelen?</a></b>
  <span id="tip1" class="show" style="display:none;">Een variabele is een kenmerk van onderzoekseenheid. Voorbeelden van variabelen in een onderzoek waarin studenten de onderzoekseenheid vormen, zouden kunnen zijn: geslacht, vooropleiding of studiekeuze. Voor verdere informatie zie het boek <i>Beschrijvende Statistiek.</i></span>

  <!-- Tip 2 -->
  <br>2 <b class="hint"><a href="#" onclick="toggleMe('tip2'); return false">Wat zijn onderzoekseenheden?</a></b>
  <span id="tip2" class="show" style="display:none;">Datgene waarover we gegevens verzamelen noemen we de onderzoekseenheid. Onderzoekseenheden hoeven niet altijd personen te zijn. Je kunt op basis van een onderzoek ook uitspraken doen over de lengte van voorpagina-artikelen in dagbladen. In dat geval zijn de voorpagina-artikelen de onderzoekseenheden, de objecten waarover je een uitspraak doet. Voor meer informatie zie het boek <i>Beschrijvende Statistiek</i>.</span>

  <!-- Tip 3 -->
  <br>3 <b class="hint"><a href="#" onclick="toggleMe('tip3'); return false">Wat zijn waarden?</a></b>
  <span id="tip3" class="show" style="display:none;"><b>Values</b>: Iedere variabele kan een bepaalde waarde aannemen. Deze waarden worden values genoemd. De variabele geslacht kan bijvoorbeeld de waarde man of vrouw aannemen en kan gecodeerd worden als 'm' en 'v' of door twee verschillende cijfers (bijvoorbeeld '1' en '2').
<br/><br/><b>Value Labels</b>: Met het commando Value Labels kunnen de waarden of codes van een variabele worden voorzien van een verklarende tekst of 'label'. In het voorbeeld van geslacht kan worden aangegeven dat 'm' man betekent en 'v' vrouw.
De datamatrix toont automatisch de opgegeven value-labels (in plaats van de ingevoerde waarden of code's) wanneer het knopje met het label (zie hieronder) ingedrukt is.</span>

  <!-- Tip 4 -->
  <br>4 <b class="hint"><a href="#" onclick="toggleMe('tip4'); return false">Wat zijn labels?</a></b>
  <span id="tip4" class="show" style="display:none;">SPSS noemt de omschrijving van de naam van de variabele een variable label. Je kunt deze omschrijving opgeven in de kolom Label in het tabblad Variable View.<br/><br/><img src="./images/labels.png"></span>

  <!-- Tip 5 -->
  <br>5 <b class="hint"><a href="#" onclick="toggleMe('tip5'); return false">Namen van variabelen</a></b>
  <span id="tip5" class="show" style="display:none;">In SPSS heeft elke variabele een unieke naam. De naam moet beginnen met een letter en er mogen geen spaties in voorkomen. Variabelenamen in SPSS zijn meestal afkortingen van de volledige naam van de variabele. De volledige naam kun je kwijt in het label van de variabele.</span>


<br/><br/><br/><br/>