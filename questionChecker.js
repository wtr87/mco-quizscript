/*
QUESTION CHECKER SCRIPT
Door: Wouter Stuifmeel
UvA - ICTO Communicatiewetenschappen
*/

function str2slug(str) {
  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç";
  var to   = "aaaaeeeeiiiioooouuuunc";
  for (var i=0, l=from.length ; i<l ; i++) {
      str = str.replace(
                new RegExp(from.charAt(i), 'g'),
                to.charAt(i)
            );
  }
  return str;
}

function checkAnswer(that, answerID, answerKey, answerType) {
  var answerInput = str2slug($(that).closest(".question").find(".answer-textarea").val());
  
  var answerRes = true;

/* If all answers in key need to be included */
  if (answerType == "all") {
    var answerKeys = answerKey.split("|");
    answerKeys.map( function(item) {
      /* Replace '&' with '|' to make optional choices within constrained choice */
      item = item.replace(new RegExp( "~", "g" ), "|");
      var answerRegExp = new RegExp(item, "i");
      var answerFound = answerRegExp.test(answerInput);
      if (answerFound === false) { answerRes = false; }
    });
/* If only one of the answers from the key needs to be right */
  } else {
    var answerRegExp = new RegExp(answerKey, "i");
    answerRes = answerRegExp.test(answerInput);
  }

  if (answerRes) {
    $(that).closest(".question").find(answerID).filter(".right").fadeToggle();
  } else {
    $(that).closest(".question").find(answerID).filter(".wrong").fadeToggle();
  }
}

$( document ).ready(function() {
  $( ".questionButton" ).click(function() {
    var that = this;

    $(that).closest(".question").find(".right").css("display", "none");
    $(that).closest(".question").find(".wrong").css("display", "none");

    var answerIDs = $(this).closest(".question").find(".answerID").map(function() {return this.value;}).get();

    answerIDs.map( function(item) {
      var itemClass = "." + item;
      var answerKey = $(that).closest(".question").find(".answerKey").filter(itemClass).val();
      var answerType = $(that).closest(".question").find(".answerType").filter(itemClass).val();
      if (!answerType) { answerType = "single";}
      checkAnswer(that, itemClass, answerKey, answerType);
    });

  });
});