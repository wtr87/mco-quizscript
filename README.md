# README #

MCO BS Quizscript

### Wat heb je nodig? ###

* Webserver
* Browser die HTML5 en JQuery ondersteunt

### Hoe gebruik je het? ###

* Zet de JS en CSS bestanden op de webserver
* Zorg ervoor dat je naar deze bestanden linkt in de html van je site
	* De link naar questionChecker.js kan het beste in de footer van de pagina, net voor de </body> tag
* De index.html file bevat twee voorbeelden. Het is de bedoeling dat elk antwoord een eigen code als class krijgt ('q1', q2a'.. etc). Deze codes mogen alles zijn, als het maar uniek is.
* De <input type='hidden'> tags bevatten de zoektermen waarnaar gezocht moet worden per antwoord
* Een goed antwoord heeft de klasse 'right' en krijgt een groene kleur, een fout antwoord heeft de klasse 'wrong' en krijgt een rode kleur.
	* Als er een vraag is wat actief naar een fout antwoord zoekt om feedback te geven, dan moet het alsnog de klasse 'right' krijgen. De kleur kan dan evt. aangepast worden met een style="background-color: #e74c3c"tag in de <div>
* Als er geen goede of foute feedback is bij een vraag, dan kan het gewoon weggelaten worden

### Testsite ###
Testomgeving voor code:
http://jsfiddle.net/LjLgq88L/